/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.cassandra.config.DatabaseDescriptor;
import org.apache.cassandra.db.Mutation;
import org.apache.cassandra.db.MutationVerbHandler;
import org.apache.cassandra.db.ReadResponse;
import org.apache.cassandra.db.WriteResponse;
import org.apache.cassandra.net.MessageIn;
import org.apache.cassandra.net.MessagingService;
import org.apache.cassandra.utils.FBUtilities;
import org.apache.cassandra.utils.Pair;
import pfouto.messaging.DownMessage;
import pfouto.messaging.Message;

public class MessageConsumer
{
    private static final Logger logger = LoggerFactory.getLogger(MessageConsumer.class);

    //                        msgType                 Node,   ID       Message

    private static final Map<Integer, Map<Pair<InetAddress, Integer>, Object>> waitingMap = new HashMap<>();

    public static void init()
    {
        waitingMap.put(Message.VERB_WRITE, new ConcurrentHashMap<>());
        waitingMap.put(Message.VERB_READ_REQUEST, new ConcurrentHashMap<>());
        waitingMap.put(Message.VERB_READ_REPLY, new ConcurrentHashMap<>());
    }

    //very ugly. TODO cleanup
    public static void receiveFromPeer(MessageIn<?> message, int id)
    {
        //Need to intercept all mutations and read request/slice/response from remote dcs...
        if (message.verb == MessagingService.Verb.MUTATION || message.parameters.get("META") != null)
        {
            InetAddress sender = message.from;
            //Forward
            if (message.verb == MessagingService.Verb.MUTATION)
            {
                try
                {
                    byte[] from = message.parameters.get(Mutation.FORWARD_FROM);
                    if (from == null)
                    {
                        byte[] forwardBytes = message.parameters.get(Mutation.FORWARD_TO);
                        if (forwardBytes != null)
                            MutationVerbHandler.forwardToLocalNodes((Mutation) message.payload, message.verb, forwardBytes, message.from);
                    }
                    else
                    {
                        sender = InetAddress.getByAddress(from);
                    }
                }
                catch (IOException e)
                {
                    logger.error("Error forwarding message, discarding");
                    return;
                }
            }

            int verb;
            if (message.verb == MessagingService.Verb.MUTATION)
            {
                verb = Message.VERB_WRITE;
            }
            else if (message.verb == MessagingService.Verb.REQUEST_RESPONSE)
            {
                verb = Message.VERB_READ_REPLY;
            }
            else
            {
                verb = Message.VERB_READ_REQUEST;
            }

            Map<Pair<InetAddress, Integer>, Object> map = waitingMap.get(verb);
            Pair<InetAddress, Integer> p = Pair.create(sender, id);

            Object o = map.putIfAbsent(p, message);
            if (o != null)
            {
                map.remove(p);
                assert o instanceof DownMessage;
                logger.debug("Executing received message: " + p);
                MessagingService.instance().receive(message, id);
            }
            else
            {
                logger.debug("Stored received message: " + p);
            }
        }
        else
        {
            MessagingService.instance().receive(message, id);
        }
    }

    static void receiveFromInternal(DownMessage m)
    {
        Map<Pair<InetAddress, Integer>, Object> map = waitingMap.get(m.getVerb());
        Pair<InetAddress, Integer> p = Pair.create(m.getFrom(), m.getId());

        Object o = map.putIfAbsent(p, m);
        if (o != null)
        {
            map.remove(p);
            assert o instanceof MessageIn<?>;
            MessageIn<?> mutation = (MessageIn<?>) o;
            logger.debug("Executing received metadata: " + m.getId());
            MessagingService.instance().receive(mutation, m.getId());
        }
        else
        {
            logger.debug("Stored received metadata: " + m.getId());
        }
    }

    private static long nMetadata(Collection<Object> values)
    {
        return values.stream().filter(o -> (o instanceof DownMessage)).count();
    }

    private static long nData(Collection<Object> values)
    {
        return values.stream().filter(o -> (o instanceof MessageIn<?>)).count();
    }
}
