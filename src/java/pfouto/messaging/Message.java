/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto.messaging;

import java.net.InetAddress;

public abstract class Message
{
    public static final int VERB_WRITE = 0;
    public static final int VERB_READ_REQUEST = 1;
    public static final int VERB_READ_REPLY = 2;

    private final InetAddress from;
    private final int verb;
    private final int code;
    private final long timestamp;

    Message(InetAddress from, int verb, int code, long timestamp)
    {
        this.from = from;
        this.verb = verb;
        this.code = code;
        this.timestamp = timestamp;
    }


    public int getVerb()
    {
        return verb;
    }

    public InetAddress getFrom()
    {
        return from;
    }

    public int getCode()
    {
        return code;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    @Override
    public String toString(){
        return "CODE: " + code + " FROM: " + from + " VERB: " + verb;
    }

}
