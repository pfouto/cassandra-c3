/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.apache.cassandra.config.DatabaseDescriptor;
import org.apache.cassandra.utils.FBUtilities;

public class InternalConnection
{

    private static final Logger logger = LoggerFactory.getLogger(InternalConnection.class);

    private static final int HIGH_PORT = 1805;
    private static final int LOW_PORT = 1806;

    private static EventLoopGroup bossGroup;
    private static EventLoopGroup inGroup;
    private static EventLoopGroup outGroup;

    private static ConcurrentMap<InetAddress, Channel> connections;

    private static InetAddress localInternal = null;

    public static void init() {

        MessageConsumer.init();
        try {
            bossGroup = new NioEventLoopGroup();
            inGroup = new NioEventLoopGroup();
            outGroup = new NioEventLoopGroup();
            connections = new ConcurrentHashMap<>();

            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, inGroup);
            b.channel(NioServerSocketChannel.class);
            b.childHandler(new ChannelInitializer<SocketChannel>() { // (4)
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline().addLast(new MessageEncoder(), new MessageDecoder(), new InternalHandler());
                    InetAddress connAddress = ch.remoteAddress().getAddress();
                    connections.put(connAddress, ch);

                    ch.closeFuture().addListener((ChannelFuture cF) -> {
                        logger.info("Connection from " + cF.channel().remoteAddress() + " closed");
                        connections.remove(((InetSocketAddress) cF.channel().remoteAddress()).getAddress());
                    });
                }
            });
            b.option(ChannelOption.SO_BACKLOG, 128);
            b.childOption(ChannelOption.SO_KEEPALIVE, true);

            // Bind and start to accept incoming connections.
            ChannelFuture f = b.bind(LOW_PORT).sync(); // (7)
            logger.info("Server socket opened!");
            f.channel().closeFuture().addListener((ChannelFuture cF) -> {
                logger.error("Server socket closed!");
                System.exit(0);
            });
        } catch (Exception e) {
            logger.error("Exception in server socket: " + e.getMessage());
            e.printStackTrace();
            outGroup.shutdownGracefully();
            inGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();

            System.exit(0);
        }
    }
    public static Channel getConnectionToInternal() throws IOException, InterruptedException
    {
        if(localInternal == null){
            Properties prop = new Properties();
            InputStream input = new FileInputStream("conf/internalConf");
            prop.load(input);
            String localDc = DatabaseDescriptor.getEndpointSnitch().getDatacenter(FBUtilities.getBroadcastAddress());
            localInternal = InetAddress.getByName(prop.getProperty(localDc));
        }

        Channel c = connections.get(localInternal);
        if (c == null) {
            c = createConnection(localInternal);
        }
        return c;
    }

    public static ConcurrentMap<InetAddress, Channel> getConnections() {
        return connections;
    }

    private static Channel createConnection(InetAddress addr) throws InterruptedException {
        Bootstrap b = new Bootstrap();
        b.group(outGroup);
        b.channel(NioSocketChannel.class);
        b.option(ChannelOption.SO_KEEPALIVE, true);

        b.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) throws Exception {
                ch.pipeline().addLast(new MessageEncoder(), new MessageDecoder(), new InternalHandler());
            }
        });
        ChannelFuture f = b.connect(addr, HIGH_PORT).sync();
        InetAddress connAddress = ((InetSocketAddress) f.channel().remoteAddress()).getAddress();
        connections.put(connAddress, f.channel());


        f.channel().closeFuture().addListener((ChannelFuture cF) -> {
            logger.info("Connection to " + cF.channel().remoteAddress() + " closed");
            connections.remove(((InetSocketAddress) cF.channel().remoteAddress()).getAddress());
        });
        return f.channel();
    }
}
