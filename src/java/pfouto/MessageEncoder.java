/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto;

import java.net.InetAddress;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import org.apache.cassandra.net.CompactEndpointSerializationHelper;
import org.apache.cassandra.utils.Pair;
import pfouto.messaging.AckMessage;
import pfouto.messaging.Message;
import pfouto.messaging.UpMessage;

public class MessageEncoder extends ChannelOutboundHandlerAdapter
{
    private static final Logger logger = LoggerFactory.getLogger(MessageEncoder.class);

    @Override
    public void write(ChannelHandlerContext ctx, Object o, ChannelPromise promise)
    {
        ByteBuf out = null;
        try
        {
            if(!(o instanceof UpMessage || o instanceof AckMessage)){
                logger.error("Unsupported object received in encoder: " + o);
                return;
            }

            out = ctx.alloc().buffer();
            ByteBufOutputStream outStream = new ByteBufOutputStream(out);
            out.writerIndex(4);

            Message m = (Message) o;

            //code
            outStream.writeInt(m.getCode());
            //from
            CompactEndpointSerializationHelper.serialize(m.getFrom(), outStream);
            //verb
            outStream.writeInt(m.getVerb());
            //timestamp
            outStream.writeLong(m.getTimestamp());

            if(o instanceof UpMessage){
                UpMessage um = (UpMessage) o;
                //targets
                Map<String, List<Pair<Integer, InetAddress>>> targets = um.getTargets();
                outStream.writeInt(targets.size());
                for (Map.Entry<String, List<Pair<Integer, InetAddress>>> entry : targets.entrySet())
                {
                    outStream.writeUTF(entry.getKey());
                    outStream.writeInt(entry.getValue().size());
                    for (Pair<Integer, InetAddress> pair : entry.getValue())
                    {
                        outStream.writeInt(pair.left);
                        CompactEndpointSerializationHelper.serialize(pair.right, outStream);
                    }
                }
                outStream.writeInt(-1);
            } else { //Ack message
                AckMessage am = (AckMessage) o;
                outStream.writeInt(am.getId());
                CompactEndpointSerializationHelper.serialize(am.getNode(), outStream);
            }

            //Size
            int index = out.writerIndex();
            out.writerIndex(0);
            out.writeInt(index-4);
            out.writerIndex(index);
            ctx.write(out, promise);
        }
        catch (Exception e)
        {
            if (out != null && out.refCnt() > 0)
                out.release(out.refCnt());
            promise.tryFailure(e);
            logger.error("Exception: " + e.getMessage());
        }
    }
}
